package com.test.foodtruckfinder.service;

import java.util.List;

import com.test.foodtruckfinder.model.FoodTruck;

public interface FoodTruckService {

	List<FoodTruck> getFilteredFoodTruckList(String location);
}
