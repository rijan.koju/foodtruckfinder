package com.test.foodtruckfinder.service.impl;

import com.test.foodtruckfinder.model.FoodTruck;
import com.test.foodtruckfinder.service.FoodTruckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FoodTruckServiceImpl implements FoodTruckService {

    @Autowired
    private RestTemplate restTemplate;

    private double calculateDistance(double lat1, double lon1, double lat2, double lon2, String unit) {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        } else {
            double theta = lon1 - lon2;
            double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2))
                    + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            if (unit.equals("K")) {
                dist = dist * 1.609344;
            } else if (unit.equals("N")) {
                dist = dist * 0.8684;
            }
            return (dist);
        }
    }

    private List<FoodTruck> getFoodTruckList() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<FoodTruck[]> entity = new HttpEntity<FoodTruck[]>(headers);

        FoodTruck[] foodTrucks = restTemplate
                .exchange("https://data.sfgov.org/resource/rqzj-sfat.json", HttpMethod.GET, entity, FoodTruck[].class)
                .getBody();
        return Arrays.asList(foodTrucks);
    }

    @Override
    public List<FoodTruck> getFilteredFoodTruckList(String location) {

        List<FoodTruck> list = getFoodTruckList();
        if (location == null || location.isEmpty())
            return list;
        Optional<FoodTruck> optFoodTruck = list.stream()
                .filter(foodTruck -> location.trim().equals(foodTruck.getAddress())).findAny();
        if (optFoodTruck.isPresent()) {
            return list.stream()
                    .filter(foodTruck -> calculateDistance(optFoodTruck.get().getLatitude(),
                            optFoodTruck.get().getLongitude(), foodTruck.getLatitude(), foodTruck.getLongitude(),
                            "K") <= 0.5)
                    .collect(Collectors.toList());
        }
        throw new NullPointerException("Location doesn't not match! Enter another location");
    }

}
