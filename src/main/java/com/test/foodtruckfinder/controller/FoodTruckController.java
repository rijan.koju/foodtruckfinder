package com.test.foodtruckfinder.controller;


import com.test.foodtruckfinder.model.FoodTruck;
import com.test.foodtruckfinder.service.FoodTruckService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(path = "/food-truck")
public class FoodTruckController {

	private static Logger logger = LoggerFactory.getLogger(FoodTruckController.class);
	
	@Autowired
	private FoodTruckService foodTruckService;

    @RequestMapping(value = "/list")
    public ResponseEntity<?> foodTruckList(@RequestParam String location) {
        try {
            return new ResponseEntity<>(foodTruckService.getFilteredFoodTruckList(location),
                    HttpStatus.OK);
        } catch (NullPointerException e) {
            logger.error("error occurred {} ", e.getMessage(), e);

            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            logger.error("error occurred {} ", e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
