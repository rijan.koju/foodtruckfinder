package com.test.foodtruckfinder.model;

import java.util.Date;

public class FoodTruck {

	private Long objectid;

	private String applicant;

	private String facilitytype;

	private Long cnn;

	private String locationdescription;

	private String address;

	private String blocklot;

	private String block;

	private String lot;

	private String permit;

	private String status;

	private String fooditems;

	private Double x;

	private Double y;

	private Double latitude;

	private Double longitude;

	private String schedule;

	private String approved;

	private Date received;

	private int priorpermit;

	private Date expirationdate;

	private Location location;

	private int computed_region_yftq_j783;

	private int computed_region_p5aj_wyqh;

	private int computed_region_rxqg_mtj9;

	private int computed_region_bh8s_q3mv;

	private int computed_region_fyvs_ahh9;

	public Long getObjectid() {
		return objectid;
	}

	public void setObjectid(Long objectid) {
		this.objectid = objectid;
	}

	public String getApplicant() {
		return applicant;
	}

	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	public String getFacilitytype() {
		return facilitytype;
	}

	public void setFacilitytype(String facilitytype) {
		this.facilitytype = facilitytype;
	}

	public Long getCnn() {
		return cnn;
	}

	public void setCnn(Long cnn) {
		this.cnn = cnn;
	}

	public String getLocationdescription() {
		return locationdescription;
	}

	public void setLocationdescription(String locationdescription) {
		this.locationdescription = locationdescription;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBlocklot() {
		return blocklot;
	}

	public void setBlocklot(String blocklot) {
		this.blocklot = blocklot;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getLot() {
		return lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}

	public String getPermit() {
		return permit;
	}

	public void setPermit(String permit) {
		this.permit = permit;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFooditems() {
		return fooditems;
	}

	public void setFooditems(String fooditems) {
		this.fooditems = fooditems;
	}

	public Double getX() {
		return x;
	}

	public void setX(Double x) {
		this.x = x;
	}

	public Double getY() {
		return y;
	}

	public void setY(Double y) {
		this.y = y;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public String getApproved() {
		return approved;
	}

	public void setApproved(String approved) {
		this.approved = approved;
	}

	public Date getReceived() {
		return received;
	}

	public void setReceived(Date received) {
		this.received = received;
	}

	public int getPriorpermit() {
		return priorpermit;
	}

	public void setPriorpermit(int priorpermit) {
		this.priorpermit = priorpermit;
	}

	public Date getExpirationdate() {
		return expirationdate;
	}

	public void setExpirationdate(Date expirationdate) {
		this.expirationdate = expirationdate;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public int getComputed_region_yftq_j783() {
		return computed_region_yftq_j783;
	}

	public void setComputed_region_yftq_j783(int computed_region_yftq_j783) {
		this.computed_region_yftq_j783 = computed_region_yftq_j783;
	}

	public int getComputed_region_p5aj_wyqh() {
		return computed_region_p5aj_wyqh;
	}

	public void setComputed_region_p5aj_wyqh(int computed_region_p5aj_wyqh) {
		this.computed_region_p5aj_wyqh = computed_region_p5aj_wyqh;
	}

	public int getComputed_region_rxqg_mtj9() {
		return computed_region_rxqg_mtj9;
	}

	public void setComputed_region_rxqg_mtj9(int computed_region_rxqg_mtj9) {
		this.computed_region_rxqg_mtj9 = computed_region_rxqg_mtj9;
	}

	public int getComputed_region_bh8s_q3mv() {
		return computed_region_bh8s_q3mv;
	}

	public void setComputed_region_bh8s_q3mv(int computed_region_bh8s_q3mv) {
		this.computed_region_bh8s_q3mv = computed_region_bh8s_q3mv;
	}

	public int getComputed_region_fyvs_ahh9() {
		return computed_region_fyvs_ahh9;
	}

	public void setComputed_region_fyvs_ahh9(int computed_region_fyvs_ahh9) {
		this.computed_region_fyvs_ahh9 = computed_region_fyvs_ahh9;
	}

}
