<!DOCTYPE html>
<html lang="en">
<head>
<title>Food Truck Finder</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
	
<style>
#map {
	height: 400px;
	width: 100%;
}

#loading-overlay {
    position: absolute;
    width: 100%;
    height:100%;
    left: 0;
    top: 0;
    display: none;
    align-items: center;
    background-color: #000;
    z-index: 999;
    opacity: 0.5;
}
.loading-icon{ position:absolute;border-top:2px solid #fff;border-right:2px solid #fff;border-bottom:2px solid #fff;border-left:2px solid #767676;border-radius:25px;width:25px;height:25px;margin:0 auto;position:absolute;left:50%;margin-left:-20px;top:50%;margin-top:-20px;z-index:4;-webkit-animation:spin 1s linear infinite;-moz-animation:spin 1s linear infinite;animation:spin 1s linear infinite;}
@-moz-keyframes spin { 100% { -moz-transform: rotate(360deg); } }
@-webkit-keyframes spin { 100% { -webkit-transform: rotate(360deg); } }
@keyframes spin { 100% { -webkit-transform: rotate(360deg); transform:rotate(360deg); } } 
</style>
</head>
<body>
	<input type="hidden" id="basicURL"
		value="${pageContext.request.contextPath}" />

	<nav class="navbar navbar-expand-sm bg-dark navbar-dark sticky-top">
		<a class="navbar-brand" href="#">Food Truck Finder</a>
	</nav>
	<div id="loading-overlay">
    	<div class="loading-icon"></div>
	</div>  
	<div class="container">
		<br>
		<h3 id="map-header">Search Food Truck</h3>
		
		<form id="search-foodtruck">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Enter Location"
					id="location">
			</div>
			<button type="submit" class="btn btn-primary">Search</button>
		</form>
		
		<br>
		<div class="row" id="map"></div>
	</div>

</body>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
	type="text/javascript"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script>
	var map;
	var PAGE_CONTEXT_PATH = "";
	$(document).ready(function() {
		PAGE_CONTEXT_PATH = $("#basicURL").val();
		$('#map').hide();
  
	});

	function initMap() {
		
		// Map options
		function zoomToMarker(foodTruck){
			var options = {
				zoom : 16,
				center : {
					lat:parseFloat(foodTruck.latitude),
					lng:parseFloat(foodTruck.longitude)
				}
			}
			 map = new google.maps.Map(document.getElementById('map'), options);
			
		}
		
		// Add Marker Function
		function addMarker(foodTruck) {
			var marker = new google.maps.Marker({
				position : {lat:parseFloat(foodTruck.latitude),lng:parseFloat(foodTruck.longitude)},
				map : map,
			});
			// Check content
			if (foodTruck.content) {
				var infoWindow = new google.maps.InfoWindow({
					content : foodTruck.content
				});
				
				 marker.addListener('click', function() {
					infoWindow.open(map, marker);
				}); 
			}
		}
	$("#search-foodtruck").submit(function(event) {
		event.preventDefault();
		$.ajax({
			method : "GET",
			url : PAGE_CONTEXT_PATH + "/food-truck/list",
			data : {
				location : $("#location").val()
			},
			dataType : "json",
			beforeSend: function(){
            	$("#loading-overlay").show();
        	},
			success : function(response) {
				if(response != null){
					$('#map').show();
					let valid = true;
					for (let foodTruck of response) {
						foodTruck.content="<h3>"+foodTruck.address+"</h3>"
						+"<br><b>Facility Type :</b> "+foodTruck.facilitytype
						+"<br><b>Block :</b> "+foodTruck.block;
						if(valid){
							zoomToMarker(foodTruck);
							valid = false;
						}
 						addMarker(foodTruck);
					}
				
				}else{
					toastr.error("No Food Trucks in given location");
					$('#map').hide();
				}
				
			},
			error : function(response) {
				toastr.error(response.responseText);
				$('#map').hide();
			},
			complete : function() {
				 $("#loading-overlay").hide();
			}
		});
	});
		
	}
	
</script>
<script async defer
	src="https://maps.googleapis.com/maps/api/js?callback=initMap"></script>

</html>


